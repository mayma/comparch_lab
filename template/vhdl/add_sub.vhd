library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity add_sub is
    port(
        a        : in  std_logic_vector(31 downto 0);
        b        : in  std_logic_vector(31 downto 0);
        sub_mode : in  std_logic;
        carry    : out std_logic;
        zero     : out std_logic;
        r        : out std_logic_vector(31 downto 0)
    );
end add_sub;

architecture synth of add_sub is
    signal s_xor : std_logic_vector(31 downto 0); -- on additionne sur 33 bits pour pouvoir récuperer l'overflow au cas ou 
    signal s_a : unsigned(32 downto 0);
    signal s_b : unsigned (32 downto 0);
    signal s_result : unsigned(32 downto 0);-- permet juste de savoir si il y a overflow ou pas 
    signal s_result2 : std_logic_vector(31 downto 0);-- 
    signal s_masque : std_logic_vector(31 downto 0);
begin
    add_s : process (a, b, sub_mode)
        s_xor <= (b xor sub);
        s_masque <= (others => '0'); -- syntaxe pour les tableaux.
        s_a <= unsigned(a);
        s_b <= unsigned (b);
        s_result = std_logic_vector(s_a + s_b); -- addition d'unsigned
        s__result2 = std_logic_vector(s_result(31 downto 0));
        -- on récupère le 33 ieme bit
        if s_result(32) = '1' then 
            carry = '1'; 
        else carry = '0'
        end if 

        if s_result2 = s_masque then -- voire si la création de std_logic 
            zero = '1';
        else zero = '0';
        end if;
        r = s_result2;   
    end process add_s;
end synth;    
    