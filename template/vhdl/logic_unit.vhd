library ieee;
use ieee.std_logic_1164.all;

entity logic_unit is
    port(
        a  : in  std_logic_vector(31 downto 0);
        b  : in  std_logic_vector(31 downto 0);
        op : in  std_logic_vector(1 downto 0);
        r  : out std_logic_vector(31 downto 0)
    );
end logic_unit;

architecture synth of logic_unit is
begin
   operation: process(op,a,b) is 
   begin 
        if op = std_logic_vector(to_unsigned(0,2)); then r <= a nor b ;
        elsif op <= std_logic_vector(to_unsigned(1,2)); then r <= a and b; 
        elsif op <= std_logic_vector(to_unsigned(2,2)); then r <= a or b ;
        else r <= a xnor b ;
        end if 
    end process operation;
end synth;

